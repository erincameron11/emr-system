import React, { Component } from "react";
import { getDoctors } from "../services/doctorsDB";
import { getTeamRoles } from "../services/teamRolesDB";
import DoctorsTable from "./doctorsTable";
import Pagination from "./common/pagination";
import SearchBox from "./common/searchBox";
import ListGroup from "./common/listGroup";
import { paginate } from "../utils/paginate";
import { Link } from "react-router-dom";
import _ from "lodash";

class Doctors extends Component {
  state = {
    doctors: [],
    roles: [],
    currentPage: 1,
    pageSize: 4,
    selectedRole: null,
    sortColumn: { path: "title", order: "asc" },
    searchQuery: "",
  };

  componentDidMount() {
    const roles = [{ _id: "", name: "All Members" }, ...getTeamRoles()];
    this.setState({ doctors: getDoctors(), roles });
  }

  handleDelete = (doctor) => {
    const doctors = this.state.doctors.filter(
      (each) => each._id !== doctor._id
    );
    this.setState({ doctors });
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleRoleSelect = (role) => {
    this.setState({ currentPage: 1, selectedRole: role, searchQuery: "" });
  };

  handleSort = (sortColumn) => {
    this.setState({
      sortColumn,
    });
  };

  handleSearch = (query) => {
    this.handleRoleSelect(null);
    this.setState({
      searchQuery: query,
    });
  };

  getPageData = () => {
    const {
      pageSize,
      currentPage,
      selectedRole,
      doctors: allDoctors,
      sortColumn,
      searchQuery,
    } = this.state;
    const filtered =
      selectedRole && selectedRole._id
        ? allDoctors.filter((d) => d.role._id === selectedRole._id)
        : allDoctors;
    const searched =
      searchQuery.length > 0
        ? allDoctors.filter((d) => {
            return (
              d._id.includes(searchQuery) ||
              d.fname.toLowerCase().includes(searchQuery.toLowerCase()) ||
              d.lname.toLowerCase().includes(searchQuery.toLowerCase())
            );
          })
        : filtered;
    const sorted = _.orderBy(searched, [sortColumn.path], [sortColumn.order]);
    const doctors = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtered.length, data: doctors };
  };

  render() {
    const { length: count } = this.state.doctors;
    const {
      pageSize,
      currentPage,
      selectedRole,
      roles,
      sortColumn,
      searchQuery,
    } = this.state;
    if (count === 0) return <p>There is no personel in the databases.</p>;

    const { totalCount, data: doctors } = this.getPageData();

    return (
      <div className="row">
        <div className="col-2">
          <ListGroup
            data={roles}
            selectedType={selectedRole}
            onItemSelect={this.handleRoleSelect}
          />
        </div>
        <div className="col">
          <Link
            to="/doctors/new"
            className="btn btn-primary"
            style={{ marginBottom: 20 }}
          >
            New Doctor
          </Link>
          <p>Showing {totalCount} doctors in the databases.</p>
          <SearchBox
            value={searchQuery}
            onChange={this.handleSearch}
            placeholder="Search by ID or Name"
          />
          <DoctorsTable
            doctors={doctors}
            sortColumn={sortColumn}
            onDelete={this.handleDelete}
            onSort={this.handleSort}
          />
          <Pagination
            itemsCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default Doctors;
