import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { saveDoctor, getDoctor } from "../services/doctorsDB";
import { getTeamRoles } from "../services/teamRolesDB";
import { getGenders } from "../services/gendersDB";

class DoctorForm extends Form {
  state = {
    data: {
      firstName: "",
      lastName: "",
      genderID: "",
      specialization: "",
      roleID: "",
      email: "",
      phone: "",
    },
    genders: [],
    roles: [],
    errors: {},
  };

  schema = {
    _id: Joi.string(),
    firstName: Joi.string().required().label("First name"),
    lastName: Joi.string().required().label("Last name"),
    genderID: Joi.string().required().label("Gender"),
    specialization: Joi.string().required().label("Specialization"),
    roleID: Joi.string().required().label("Role"),
    email: Joi.string().label("Email"),
    phone: Joi.number()
      .integer()
      .min(2000000000)
      .max(9000000000)
      .label("Phone number"),
    join_date: Joi.string(),
  };

  componentDidMount() {
    const roles = getTeamRoles();
    const genders = getGenders();
    this.setState({ roles, genders });
    const doctorId = this.props.match.params.id;
    if (doctorId === "new") return;
    const doctor = getDoctor(doctorId);
    if (!doctor) return this.props.history.replace("/not-found");
    this.setState({ data: this.mapToViewMedel(doctor) });
  }

  mapToViewMedel(doctor) {
    return {
      _id: doctor._id,
      firstName: doctor.fname,
      lastName: doctor.lname,
      genderID: doctor.gender._id,
      specialization: doctor.spec,
      roleID: doctor.role._id,
      email: doctor.email,
      phone: doctor.phone,
      join_date: doctor.join_date,
    };
  }

  doSubmit = () => {
    saveDoctor(this.state.data);
    this.props.history.push("/doctors");
  };

  render() {
    const { match } = this.props;
    const { join_date } = this.state.data;
    const { genders, roles } = this.state;
    return (
      <React.Fragment>
        <form className="container" onSubmit={this.handleSubmit}>
          <h2>Doctor ID: {match.params.id}</h2>
          <h4>Date Since: {join_date}</h4>
          <div class="row">
            {this.renderInput("firstName", "First Name")}
            {this.renderInput("lastName", "Last Name")}
          </div>
          <div class="row">
            {this.renderSelect("genderID", "Gender", genders)}
          </div>
          <div class="row">
            {this.renderInput("phone", "Phone", "number")}
            {this.renderInput("email", "Email", "email")}
          </div>
          <div class="row">
            {this.renderSelect("roleID", "Role", roles)}
            {this.renderInput("specialization", "Specialization")}
          </div>
          <div class="row">{this.renderButton("Save", "primary")}</div>
        </form>
      </React.Fragment>
    );
  }
}

export default DoctorForm;
