import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { savePatient, getPatient } from "../services/patientsDB";
import { getDoctors } from "../services/doctorsDB";
import { getGenders } from "../services/gendersDB";

class PatientForm extends Form {
  state = {
    data: {
      healthID: "",
      firstName: "",
      middleName: "",
      lastName: "",
      genderID: "",
      DOB: "",
      email: "",
      phone: "",
      doctorID: "",
    },
    doctors: [],
    genders: [],
    errors: {},
  };

  schema = {
    _id: Joi.string(),
    healthID: Joi.string().min(10).max(10).required().label("Health ID"),
    registrationDate: Joi.string(),
    firstName: Joi.string().required().label("First name"),
    middleName: Joi.string().required().label("Middle name"),
    lastName: Joi.string().required().label("Last name"),
    genderID: Joi.string().required().label("Gender"),
    DOB: Joi.string().label("DOB"),
    email: Joi.string().label("Email"),
    phone: Joi.number()
      .integer()
      .min(2000000000)
      .max(9000000000)
      .label("Phone number"),
    doctorID: Joi.string().required().label("Doctor"),
  };

  componentDidMount() {
    const doctors = getDoctors();
    const genders = getGenders();
    this.setState({ doctors, genders });
    const patientId = this.props.match.params.id;
    if (patientId === "new") return;
    const patient = getPatient(patientId);
    if (!patient) return this.props.history.replace("/not-found");
    this.setState({ data: this.mapToViewMedel(patient) });
  }

  mapToViewMedel(patient) {
    return {
      _id: patient._id,
      healthID: patient.healthID,
      registrationDate: patient.regDate,
      firstName: patient.fname,
      middleName: patient.mname,
      lastName: patient.lname,
      genderID: patient.gender._id,
      DOB: patient.DOB,
      email: patient.email,
      phone: patient.phone,
      doctorID: patient.doctor._id,
    };
  }

  doSubmit = () => {
    savePatient(this.state.data);
    this.props.history.push("/patients");
  };

  render() {
    const { match } = this.props;
    const { data, genders, doctors } = this.state;
    return (
      <React.Fragment>
        <form className="container" onSubmit={this.handleSubmit}>
          <h2>Patient: {match.params.id}</h2>
          <h4>Registerated Since: {data.registrationDate}</h4>
          <div class="row">{this.renderInput("healthID", "Health ID")}</div>
          <div class="row">
            {this.renderInput("firstName", "First Name")}
            {this.renderInput("lastName", "Last Name")}
          </div>
          <div class="row">{this.renderInput("middleName", "Middle Name")}</div>
          <div class="row">
            {this.renderSelect("genderID", "Gender", genders)}
            {this.renderInput("DOB", "Date of Birth", "date")}
          </div>
          <div class="row">
            {this.renderInput("phone", "Phone", "number")}
            {this.renderInput("email", "Email", "email")}
          </div>
          <div class="row">
            {this.renderSelect("doctorID", "Doctor", doctors)}
          </div>
          <div class="row">{this.renderButton("Save", "primary")}</div>
        </form>
      </React.Fragment>
    );
  }
}

export default PatientForm;
