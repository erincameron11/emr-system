export const roles = [
  { _id: "R0", name: "Admin" },
  { _id: "R1", name: "Doctor" },
  { _id: "R2", name: "Nurse" },
  { _id: "R3", name: "Reception" },
];

export function getTeamRoles() {
  return roles.filter((d) => d);
}

export function getTeamRole(id) {
  return roles.find((d) => d._id === id);
}
