import _ from "lodash";
import * as gendersAPI from "./gendersDB";
import * as rolesAPI from "./teamRolesDB";
import { incrementStr } from "../utils/incrementStr";

export const doctors = [
  {
    _id: "421524",
    fname: "John",
    lname: "Thorman",
    name() {
      return this.fname + this.lname;
    },
    gender: { _id: "1001", name: "Male" },
    phone: 6478589635,
    email: "John.T@medicare.ca",
    join_date: "2016-02-05",
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    spec: "X-ray",
    role: { _id: "R1", name: "Doctor" },
  },
  {
    _id: "236134",
    fname: "Candy",
    lname: "Yellow",
    name() {
      return this.fname + this.lname;
    },
    gender: { _id: "1002", name: "Female" },
    phone: 2893531586,
    email: "Candy.y@medicare.ca",
    join_date: "2014-02-02",
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    spec: "X-ray",
    role: { _id: "R1", name: "Doctor" },
  },
  {
    _id: "546127",
    fname: "Jenny",
    lname: "Gordan",
    name() {
      return this.fname + this.lname;
    },
    gender: { _id: "1002", name: "Female" },
    phone: 4167548858,
    email: "Jenny.g@medicare.ca",
    join_date: "2018-09-09",
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    spec: "X-ray",
    role: { _id: "R1", name: "Doctor" },
  },
  {
    _id: "546677",
    fname: "Westley",
    lname: "Tan",
    name() {
      return this.fname + this.lname;
    },
    gender: { _id: "1001", name: "Male" },
    phone: 6478219110,
    email: "Westley.t@medicare.ca",
    join_date: "2020-08-09",
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    spec: "X-ray",
    role: { _id: "R1", name: "Doctor" },
  },
  {
    _id: "546679",
    fname: "Jack",
    lname: "Fly",
    name() {
      return this.fname + this.lname;
    },
    gender: { _id: "1001", name: "Male" },
    phone: 4165259574,
    email: "Jack.f@medicare.ca",
    join_date: "2020-08-09",
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    spec: "X-ray",
    role: { _id: "R2", name: "Nurse" },
  },
];

export function getDoctors() {
  const getDoctors = [...doctors];
  getDoctors.map((d) => (d.name = `${d.fname} ${d.lname}`));
  return getDoctors;
}

export function getDoctor(doctorId) {
  return doctors.find((d) => d._id === doctorId);
}

export function saveDoctor(doctor) {
  let doctorInDb = doctors.find((d) => d._id === doctor._id) || {};
  doctorInDb.fname = _.capitalize(doctor.firstName);
  doctorInDb.lname = _.capitalize(doctor.lastName);
  doctorInDb.gender = gendersAPI.genders.find((d) => d._id === doctor.genderID);
  doctorInDb.phone = doctor.phone;
  doctorInDb.email = doctor.email.toLowerCase();
  doctorInDb.spec = _.capitalize(doctor.specialization);
  doctorInDb.role = rolesAPI.roles.find((r) => r._id === doctor.roleID);
  doctorInDb.join_date = doctorInDb.join_date
    ? doctorInDb.join_date
    : new Date().toISOString().slice(0, 10);
  if (!doctorInDb._id) {
    doctorInDb._id = incrementStr(doctors[doctors.length - 1]._id, 1);
    doctors.push(doctorInDb);
  }
  return doctorInDb;
}

export function deleteDoctor(id) {
  let doctorInDb = doctors.find((d) => d._id === id);
  doctors.splice(doctors.indexOf(doctorInDb), 1);
  return doctorInDb;
}
