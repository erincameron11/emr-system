import _ from "lodash";
import * as doctorsAPI from "./doctorsDB";
import * as gendersAPI from "./gendersDB";
import { incrementStr } from "../utils/incrementStr";

const patients = [
  {
    _id: "101",
    healthID: "2030423484",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Leanne",
    mname: "Lina",
    lname: "Graham",
    gender: { _id: "1001", name: "Male" },
    email: "Leanne@april.biz",
    phone: 7707368031,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "421524", name: "John Thorman" },
    healthDetails: "healthy",
  },
  {
    _id: "102",
    healthID: "2004259968",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Ervin",
    mname: "Lina",
    lname: " Howell",
    gender: { _id: "1002", name: "Female" },
    email: "Ervin@melissa.tv",
    phone: 6106926593,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "546127", name: "Jenny Gordan" },
    healthDetails: "healthy",
  },
  {
    _id: "103",
    healthID: "2091666784",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Clementine",
    mname: "Macron",
    lname: "Bauch",
    gender: { _id: "1001", name: "Male" },
    email: "Clementine@yesenia.net",
    phone: 4631234447,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "236134", name: "Candy Yellow" },
    healthDetails: "healthy",
  },
  {
    _id: "104",
    healthID: "2047245067",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Patricia",
    mname: "Le",
    lname: "Lebsack",
    gender: { _id: "1002", name: "Female" },
    email: "Lebsack@kory.org",
    phone: 4931709623,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "421524", name: "John Thorman" },
    healthDetails: "healthy",
  },
  {
    _id: "105",
    healthID: "2014975010",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Chelsey",
    mname: "Ha",
    lname: "Dietrich",
    gender: { _id: "1001", name: "Male" },
    email: "Chelsey06@hotmail.ca",
    phone: 2549541289,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "236134", name: "Candy Yellow" },
    healthDetails: "healthy",
  },
  {
    _id: "106",
    healthID: "2048861765",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Dennis",
    mname: "Fly",
    lname: "Schulist",
    gender: { _id: "1002", name: "Female" },
    email: "Schulisth@jtz.info",
    phone: 4779358478,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "236134", name: "Candy Yellow" },
    healthDetails: "healthy",
  },
  {
    _id: "107",
    healthID: "2019243833",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Kurtis",
    mname: "Cla",
    lname: "Weissnat",
    gender: { _id: "1001", name: "Male" },
    email: "Weissnat66@gmail.sg",
    phone: 2890676132,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "546127", name: "Jenny Gordan" },
    healthDetails: "healthy",
  },
  {
    _id: "108",
    healthID: "2096803637",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Nicholas",
    mname: "O.",
    lname: "Runolfsdottir",
    gender: { _id: "1002", name: "Female" },
    email: "Runolfsdottir@outlook.ca",
    phone: 5864936943,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "421524", name: "John Thorman" },
    healthDetails: "healthy",
  },
  {
    _id: "109",
    healthID: "2094443637",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Greg",
    mname: "Obama",
    lname: "Greenwood",
    gender: { _id: "1003", name: "LGBT+" },
    email: "getgreg@outlook.com",
    phone: 4168587595,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "546127", name: "Jenny Gordan" },
    healthDetails: "healthy",
  },
  {
    _id: "110",
    healthID: "2023603677",
    regDate: "2020-01-01",
    DOB: "1985-01-01",
    fname: "Jason",
    mname: "Omaba",
    lname: "Hunter",
    gender: { _id: "1001", name: "Male" },
    email: "Jasonflysky@outlook.com",
    phone: 6478589641,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "236134", name: "Candy Yellow" },
    healthDetails: "healthy",
  },
  {
    _id: "111",
    healthID: "2012303646",
    DOB: "1985-01-01",
    fname: "Babara",
    mname: "J.",
    lname: "Brownbear",
    gender: { _id: "1001", name: "Male" },
    email: "BrownbearNova@bell.ca",
    phone: 5864936943,
    address: {
      unit: "",
      strNum: "4700",
      strName: "Keele St",
      city: "North York",
      postal: "M3J1P3",
    },
    doctor: { _id: "546127", name: "Jenny Gordan" },
    healthDetails: "healthy",
  },
];

export function getPatients() {
  return patients;
}

export function getPatient(id) {
  return patients.find((p) => p._id === id);
}

export function savePatient(patient) {
  let patientInDb = patients.find((p) => p._id === patient._id) || {};
  patientInDb.healthID = patient.healthID;
  patientInDb.regDate = patientInDb.regDate
    ? patientInDb.regDate
    : new Date().toISOString().slice(0, 10);
  patientInDb.fname = _.capitalize(patient.firstName);
  patientInDb.mname = _.capitalize(patient.middleName);
  patientInDb.lname = _.capitalize(patient.lastName);
  patientInDb.gender = gendersAPI.genders.find(
    (g) => g._id === patient.genderID
  );
  patientInDb.DOB = patient.DOB;
  patientInDb.email = patient.email.toLowerCase();
  patientInDb.phone = patient.phone;
  patientInDb.doctor = doctorsAPI.doctors.find(
    (d) => d._id === patient.doctorID
  );
  if (!patientInDb._id) {
    patientInDb._id = incrementStr(patients[patients.length - 1]._id, 1);
    patients.push(patientInDb);
  }

  return patientInDb;
}

export function deletePatient(id) {
  let patientInDb = patients.find((p) => p._id === id);
  patients.splice(patients.indexOf(patientInDb), 1);
  return patientInDb;
}
