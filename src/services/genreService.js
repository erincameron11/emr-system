import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndPoint = apiUrl + "/doctors";

export function getGenres() {
  try {
    return http.get(apiEndPoint);
  } catch (err) {
    console.error("Opps!", err);
  }
}
