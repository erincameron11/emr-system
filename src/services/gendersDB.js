export const genders = [
  { _id: "1001", name: "Male" },
  { _id: "1002", name: "Female" },
  { _id: "1003", name: "LGBT+" },
];

export function getGenders() {
  return genders.filter((g) => g);
}
